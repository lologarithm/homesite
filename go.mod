module gitlab.com/lologarithm/homesite

go 1.13

require (
	github.com/andybalholm/brotli v1.0.0
	github.com/mattrobenolt/gocql v0.0.0-20130828033103-56c5a46b65ee
	golang.org/x/crypto v0.22.0
)
