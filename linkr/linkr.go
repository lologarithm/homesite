package linkr

import "time"

type Linkr struct {
	Posts map[string]Post
}

type Post struct {
	Name string
	Link string
	Date string
}

func New() *Linkr {
	data := map[string]Post{}

	data["blah"] = Post{
		Name: "Funny Link",
		Link: "https://reddit.com/r/funny",
		Date: time.Now().Format("Mon Jan _2 2006 15:04"),
	}
	return &Linkr{
		Posts: data,
	}
}
