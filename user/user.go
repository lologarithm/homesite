package user

import (
	"bytes"
	"errors"
	"io/ioutil"
	"log"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type LoginHandler func(username string, pass []byte, cookie string) (User, error)

var NoSuchUser = errors.New("no such user or password incorrect")
var ExpiredSession = errors.New("user session expired")

func Manage() LoginHandler {
	users := map[string]*User{}
	sessions := map[string]*Session{}

	// TODO: When I have time replace this with a real database?
	udata, err := ioutil.ReadFile("users")
	if err != nil {
		log.Printf("No users loaded")
		return func(username string, pass []byte, cookie string) (User, error) {
			return User{}, NoSuchUser
		}
	}

	ulist := bytes.Split(udata, []byte{'\n'})
	for _, userline := range ulist {
		if len(userline) < 3 {
			continue
		}
		uparts := bytes.Split(userline, []byte{'='})
		lvl, _ := strconv.Atoi(string(uparts[2]))

		// Obv we will have it pre-encrypted in the DB...
		// but this is to facilitate faster development and manual user management.
		pwd, err := bcrypt.GenerateFromPassword(uparts[1], 0)
		if err != nil {
			log.Fatalf("failed to encrypt password, crashing now: %s", err)
		}
		un := string(uparts[0])
		users[un] = &User{
			Name:     un,
			Password: pwd,
			Access:   lvl,
		}
	}

	return func(username string, pass []byte, sessID string) (User, error) {
		// Always check for a sess first
		if sess, ok := sessions[sessID]; sessID != "" && ok {
			if time.Now().After(sess.Expires) {
				delete(sessions, sessID)
				return *sess.User, ExpiredSession
			}
			return *sess.User, nil
		}

		// Next try to login the user
		if user, ok := users[username]; username != "" && ok {
			if err := bcrypt.CompareHashAndPassword(user.Password, pass); err == nil {
				if sessID != "" {
					sessions[sessID] = &Session{
						User:    user,
						Expires: time.Now().Add(time.Hour),
					}
				}
				return *user, nil
			}
		}

		// Finally this means that we were unsuccessful
		return User{}, NoSuchUser
	}
}

type Session struct {
	User    *User
	Expires time.Time
}

type User struct {
	Name     string
	Password []byte
	Access   int
}
