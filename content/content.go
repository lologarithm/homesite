package content

import (
	"io/ioutil"
	"path"
)

func LoadContent(contentDirectory, filePath string) string {
	loadFile := path.Join(contentDirectory, filePath)
	fd, err := ioutil.ReadFile(loadFile)
	if err != nil {
		panic(err)
	}
	return string(fd)
}
