package main

import (
	"encoding/json"
	"flag"
	"io"
	"log"
	"net/http"
	"os/exec"
	"path"

	"gitlab.com/lologarithm/homesite/handler"
)

func main() {
	addr := flag.String("addr", ":443", "host address")
	domain := flag.String("domain", "echols.io", "domain for ssl")
	contentDirectory := flag.String("content", "./content/", "location of content")
	hookToken := flag.String("hook", "", "secret token for use with webhooks")
	flag.Parse()

	// TODO: make this use a config system of some kind (file/KV store/etc)
	proxies := map[string]string{
		"keep":   "http://192.168.1.50:3179",
		"refuge": "http://192.168.1.50:8005",
		"stats":  "http://192.168.1.50:3000",
		"hooks":  "http://192.168.1.50:8006",
		"timer":  "http://192.168.1.50:3335",
		"dice":   "http://192.168.1.50:9321",
	}

	go runHooks(*hookToken, *contentDirectory)

	handler.Serve(*addr, *domain, *contentDirectory, proxies)
}

func runHooks(hookToken, contentDirectory string) {

	type hookData struct {
		ObjectKind string `json:"object_kind"`
		EventName  string `json:"event_name"`
		EventType  string `json:"event_type"`
	}
	// create `ServerMux`
	mux := http.NewServeMux()

	// create a default route handler
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("X-Gitlab-Token") != hookToken {
			http.NotFound(w, r)
			return
		}
		body, _ := io.ReadAll(r.Body)
		reqData := hookData{}

		json.Unmarshal(body, &reqData)

		if reqData.ObjectKind != "push" && reqData.ObjectKind != "merge_request" {
			log.Printf("Unknown Event Type: %s", reqData)
			return
		}

		cmd := exec.Command("git", "pull", "origin", "master")
		cmd.Dir = path.Join(contentDirectory, "..")

		res, err := cmd.CombinedOutput()
		if err != nil {
			log.Printf("Failed to execute git update: %s", err)
			log.Printf("Output:\n%s", string(res))
			return
		}
		log.Printf("Push Event %#v, Updated To Latest Master.", reqData)
	})

	// create new server
	server := http.Server{
		Addr:    ":8006",
		Handler: mux,
	}

	server.ListenAndServe()
}
