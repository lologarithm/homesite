package handler

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"strings"
	"text/template"

	"gitlab.com/lologarithm/homesite/blog"
	"gitlab.com/lologarithm/homesite/user"
)

type Content struct {
	Title string
	Date  string
	Link  string
	Body  string
}

type pageContents struct {
	Entries []Content
	Nav     []NavEle
	Style   string
	User    string
}

type NavEle struct {
	Name  string
	Link  string
	Class string
}

// Static will serve the given file from the path in the url.
func Static(w http.ResponseWriter, r *http.Request, path []string, u user.User) {
	if len(path) < 2 || !strings.Contains(path[len(path)-1], ".") {
		// Only serve actual files here.
		return
	}
	http.ServeFile(w, r, strings.Join(path, "/"))
}

func root(pc pageContents, contentDirectory string, w http.ResponseWriter) {
	maintmpl, err := ioutil.ReadFile(path.Join(contentDirectory, "main.tmpl.html"))
	if err != nil {
		w.Write([]byte("<html>Failed to load site data. This is pretty bad.</html>"))
		log.Printf("Failed to load main template: %s", err)
		return
	}
	tmpl, err := template.New("main").Parse(string(maintmpl))
	if err != nil {
		w.Write([]byte("<html>Failed to load site data. This is pretty bad.</html>"))
		log.Printf("Failed to load main template: %s", err)
		return
	}

	tmpl.Execute(w, pc)
}

// Root serves up the root site and is the default handler for everything not specially handled.
func createContents(u user.User, contentDirectory string) pageContents {
	// Inline the main CSS file... because why make them download two resources when we already regenerate the main html every time.
	style, _ := ioutil.ReadFile(path.Join(contentDirectory, "..", "static/css/bensite.css"))
	style = bytes.Replace(bytes.Replace(style, []byte{9}, []byte{}, -1), []byte{10}, []byte{}, -1)

	pc := pageContents{
		Entries: []Content{},
		Nav: []NavEle{
			{Name: "Home", Link: "/"}, {Name: "Blog", Link: "/blog"}, {Name: "Projects", Link: "/proj"},
		},
		Style: string(style),
		User:  u.Name,
	}
	if u.Name != "" {
		pc.Nav = append(pc.Nav, NavEle{Name: "Linkr", Link: "/linkr"})
	}
	return pc
}

func loadBlogEntries(contentDirectory string, path []string) []Content {
	if len(path) > 1 && path[1] == "entry" {
		// Serve single blog entry
		ent := blog.LoadEntry(contentDirectory, path[2])
		return []Content{{
			Title: ent.Title,
			Date:  ent.Date,
			Link:  `<a href="/blog/entry/` + ent.Loc + `">`,
			Body:  ent.Content,
		}}
	}

	// serve blog!
	ents := blog.LoadEntrys(contentDirectory)
	contents := make([]Content, len(ents))
	for i, ent := range ents {
		contents[i] = Content{
			Title: ent.Title,
			Date:  ent.Date,
			Link:  `<a href="/blog/entry/` + ent.Loc + `">`,
			Body:  ent.Short,
		}
	}
	return contents
}

// To implement caching and monitoring for changes, use fsnotify.
// watcher, err := fsnotify.NewWatcher()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer watcher.Close()
//
// 	done := make(chan bool)
// 	go func() {
// 		for {
// 			select {
// 			case event, ok := <-watcher.Events:
// 				if !ok {
// 					return
// 				}
// 				log.Println("event:", event)
// 				if event.Op&fsnotify.Write == fsnotify.Write {
// 					log.Println("modified file:", event.Name)
// 				}
// 			case err, ok := <-watcher.Errors:
// 				if !ok {
// 					return
// 				}
// 				log.Println("error:", err)
// 			}
// 		}
// }()
