package handler

import (
	"bytes"
	"io"
	"net/http"
	"strings"
)

// HeaderContentType is the Content-Type header constant
const HeaderContentType = "Content-Type"

type brotliResponseWriter struct {
	io.Writer

	buf        *bytes.Buffer
	statusCode int
}

func (w *brotliResponseWriter) Write(b []byte) (int, error) {
	// Just add to the buffer.
	return w.buf.Write(b)
}

func (w *brotliResponseWriter) WriteHeader(statusCode int) {
	w.statusCode = statusCode
}

func (w *brotliResponseWriter) WriteFinal(writer http.ResponseWriter) {
	encoding := strings.ToLower(writer.Header().Get("Content-Encoding"))
	if w.statusCode == 0 {
		w.statusCode = 200
	}
	bytes := w.buf.Bytes()
	if writer.Header().Get(HeaderContentType) == "" {
		writer.Header().Set(HeaderContentType, http.DetectContentType(bytes))
	}

	if w.statusCode != 200 || encoding == "gzip" || encoding == "br" {
		// Don't re-compress content that was already compressed.
		writer.WriteHeader(w.statusCode)
		writer.Write(w.buf.Bytes())
		return
	}

	writer.Header().Set("Content-Encoding", "br")
	writer.Header().Del("Content-Length")
	w.Writer.Write(bytes)
}

func (w *brotliResponseWriter) Header() http.Header {
	return map[string][]string{}
}
