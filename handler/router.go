package handler

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"path"
	"strings"

	"github.com/mattrobenolt/gocql/uuid"
	"gitlab.com/lologarithm/homesite/content"
	"gitlab.com/lologarithm/homesite/linkr"
	"gitlab.com/lologarithm/homesite/user"
)

// NewRouter contructs a router for the blog
func NewRouter(contentDirectory string) http.HandlerFunc {
	loginHandler := user.Manage()
	linkHandler := linkr.New()
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/login" {
			err := r.ParseForm()
			// log.Printf("FORM: %#v %#v", r.Form, r.PostForm)
			if err != nil {
				log.Printf("Failed to parse the form: %s", err)
				http.Redirect(w, r, "/", http.StatusMovedPermanently)
				return
			}
			u := r.PostForm.Get("user")
			p := r.PostForm.Get("pwd")
			sid := uuid.RandomUUID()
			_, err = loginHandler(u, []byte(p), sid.String())
			if err != nil {
				log.Printf("Failed to login user: %s", err.Error())
				http.Redirect(w, r, "/", http.StatusForbidden)
				return
			}
			http.SetCookie(w, &http.Cookie{Name: "auth", Value: sid.String()})
			http.Redirect(w, r, "/", http.StatusMovedPermanently)
			return
		}

		cookie, err := r.Cookie("auth")
		var user user.User
		if err == nil {
			user, _ = loginHandler("", nil, cookie.Value)
		}

		Route(w, r, contentDirectory, user, linkHandler)
	}
}

// Route will handle figuring out what page to serve based on the URL
// of the request.
func Route(w http.ResponseWriter, r *http.Request, contentDirectory string, u user.User, linkH *linkr.Linkr) {
	const selCSSClass = "sel"

	filePath := strings.Split(strings.TrimPrefix(r.URL.Path, "/"), "/")
	switch filePath[0] {
	case "static":
		Static(w, r, filePath, u)
	case "robots.txt":
		http.ServeFile(w, r, path.Join(contentDirectory, "..", "robots.txt"))
	default:
		pc := createContents(u, contentDirectory)
		switch filePath[0] {
		case "blog":
			pc.Entries = loadBlogEntries(contentDirectory, filePath)
			pc.Nav[1].Class = selCSSClass
		case "proj":
			proj := content.LoadContent(contentDirectory, "proj/proj.html")
			pc.Entries = []Content{
				{Body: proj},
			}
			pc.Nav[2].Class = selCSSClass
		case "linkr":
			if u.Access > 1 {
				ltemp := content.LoadContent(contentDirectory, "linkr/linkr.html")
				data := &bytes.Buffer{}
				linkbody, err := template.New("linktemplate").Parse(ltemp)
				if err != nil {
					log.Printf("Failed to execute linkr: %s", err.Error())
					return
				}
				linkbody.Execute(data, linkH.Posts)
				pc.Entries = []Content{
					{Body: data.String()},
				}
				pc.Nav[3].Class = selCSSClass
			}
		default:
			home := content.LoadContent(contentDirectory, "home/home.html")
			pc.Entries = append(pc.Entries, Content{
				Body: home,
			})
			pc.Nav[0].Class = selCSSClass
		}
		root(pc, contentDirectory, w)
	}
}
