package handler

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"

	"golang.org/x/crypto/acme/autocert"
)

// Serve launches the server as either http or https based on addr.
//
//	This is a blocking function and will not exit until the server stops or crashes.
func Serve(addr, domain, contentDirectory string, proxies map[string]string) {
	log.Printf("Launching server for domain(%s) on %s", domain, addr)
	log.Printf("proxies: %#v", proxies)
	log.Printf("content: %s", contentDirectory)

	router := NewRouter(contentDirectory) // this servers router
	proxyFunc := proxy(domain, proxies)   // proxies for other http servers
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if proxyFunc(w, r) {
			return
		}
		router(w, r)
	})

	domainTrim := domain
	if domainTrim[0] != '.' {
		domainTrim = "." + domain
	}

	if addr == ":443" {
		certManager := &autocert.Manager{
			Prompt: autocert.AcceptTOS,
			HostPolicy: func(_ context.Context, host string) error {
				if !strings.HasSuffix(host, domain) {
					return fmt.Errorf("acme/atocert: host %q not configured in HostPolicy", host)
				}

				// direct host is ok
				if host == domain {
					return nil
				}

				// no domain or www is ok
				subdomain := strings.TrimSuffix(host, domainTrim)
				if subdomain == "" || subdomain == "www" {
					return nil
				}

				// validate subdomain in my proxies list.
				if _, ok := proxies[subdomain]; !ok {
					return fmt.Errorf("acme/atocert: subdomain %q not configured in HostPolicy", subdomain) // don't allow arbitrary subdomains or else lets encryt gets upset
				}

				return nil
			},
			Cache: autocert.DirCache("certs"), //Folder for storing certificates
		}

		server := &http.Server{
			Addr: ":https",
			TLSConfig: &tls.Config{
				GetCertificate: certManager.GetCertificate,
				MinVersion:     tls.VersionTLS12, // improves cert reputation score at https://www.ssllabs.com/ssltest/
			},
		}

		go func() {
			log.Fatal(http.ListenAndServe(":http", &intranetRouter{router: router, certHandler: certManager.HTTPHandler(nil)}))
		}()
		log.Fatal(server.ListenAndServeTLS("", "")) //Key and cert are coming from Let's Encrypt
	} else {
		// When running locally (probably for development/debugging) just use http.
		log.Fatal(http.ListenAndServe(addr, nil))
	}
}

// intranetRouter allows http serving from internal IP addresses
type intranetRouter struct {
	certHandler http.Handler
	router      http.HandlerFunc
}

var internalMask = net.IPv4Mask(255, 255, 0, 0)
var internalAddr = net.IPv4(192, 168, 0, 0)

func (i *intranetRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ip := net.ParseIP(strings.Split(r.RemoteAddr, ":")[0])
	if ip != nil {
		masked := ip.Mask(internalMask)
		if masked.Equal(internalAddr) {
			// If on the 192.168.X.X range, use http!
			i.router.ServeHTTP(w, r)
			return
		}
	}
	i.certHandler.ServeHTTP(w, r)
}
