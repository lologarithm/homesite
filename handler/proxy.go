package handler

import (
	"bytes"
	"io"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"

	"github.com/andybalholm/brotli"
)

type proxyHandler func(w http.ResponseWriter, r *http.Request) bool
type proxyEntry struct {
	srv      *httputil.ReverseProxy
	url      *url.URL
	Compress bool
}

// proxy accepts a config map (maps subdomain->route) and returns handler func
func proxy(baseDomain string, config map[string]string) proxyHandler {
	proxies := map[string]proxyEntry{}
	for k, v := range config {
		url, err := url.Parse(v)
		if err != nil {
			panic(err)
		}
		compress := true
		proxies[k] = proxyEntry{
			srv:      httputil.NewSingleHostReverseProxy(url),
			url:      url,
			Compress: compress,
		}
	}

	domainTrim := baseDomain
	if domainTrim[0] != '.' {
		domainTrim = "." + domainTrim
	}
	return func(w http.ResponseWriter, r *http.Request) bool {
		subdomain := strings.TrimSuffix(r.Host, domainTrim)
		proxy, ok := proxies[subdomain]
		if !ok {
			if subdomain == "" || subdomain == "www" || subdomain == "echols.io" {
				return false
			}
			return true // don't route arbitrary subdomains
		}

		r.Header.Set("X-Echols-A", r.RemoteAddr)
		if IsWebSocket(r) {
			dialer := net.Dialer{KeepAlive: time.Second * 10}
			d, err := dialer.Dial("tcp", proxy.url.Host)
			if err != nil {
				log.Printf("ERROR: dialing websocket backend %s: %v\n", "localhost:3179", err)
				http.Error(w, "Error contacting backend server.", 500)
				return true
			}
			hj, ok := w.(http.Hijacker)
			if !ok {
				log.Println("ERROR: Not Hijackable")
				http.Error(w, "Internal Error: Not Hijackable", 500)
				return true
			}
			nc, _, err := hj.Hijack()
			if err != nil {
				log.Printf("ERROR: Hijack error: %v\n", err)
				return true
			}
			defer nc.Close()
			defer d.Close()

			// copy the request to the target first
			err = r.Write(d)
			if err != nil {
				log.Printf("ERROR: copying request to target: %v\n", err)
				return true
			}

			errc := make(chan error, 2)
			cp := func(dst io.Writer, src io.Reader) {
				_, err := io.Copy(dst, src)
				errc <- err
			}
			go cp(d, nc)
			go cp(nc, d)
			<-errc
			return true
		}

		// Compress the JS files...
		if proxy.Compress && strings.Contains(r.Header.Get("Accept-Encoding"), "br") && strings.HasSuffix(r.URL.String(), ".js") {
			compressor := brotli.NewWriter(w)
			defer func() {
				err := compressor.Close()
				if err != nil {
					log.Printf("compressor.Close: %s\n", err)
				}
			}()
			writer := &brotliResponseWriter{buf: &bytes.Buffer{}, Writer: compressor}
			proxy.srv.ServeHTTP(writer, r)
			w.Header().Set(HeaderContentType, "text/javascript")
			writer.WriteFinal(w)
		} else {
			proxy.srv.ServeHTTP(w, r)
		}

		return true
	}
}

// IsWebSocket determines if a given request is a websocket upgrade request.
func IsWebSocket(req *http.Request) bool {
	isUpgrade := false
	connHeaders := req.Header["Connection"]
	for _, ch := range connHeaders {
		if strings.Contains(strings.ToLower(ch), "upgrade") {
			upgradeHeaders := req.Header["Upgrade"]
			if len(upgradeHeaders) > 0 {
				isUpgrade = (strings.ToLower(upgradeHeaders[0]) == "websocket")
			}
		}
	}
	return isUpgrade
}
