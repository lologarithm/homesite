package blog

import (
	"bytes"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

type BlogEntry struct {
	Title    string
	Date     string
	DateTime time.Time
	Loc      string
	Short    string
	Content  string
}

var newline = []byte{10}

func LoadEntry(contentDirectory, filePath string) BlogEntry {
	loadFile := path.Join(contentDirectory, "blog", filePath)
	return loadEntry(loadFile)
}

func loadEntry(loc string) BlogEntry {
	fd, err := ioutil.ReadFile(loc)
	if err != nil {
		panic(err)
	}
	blogparts := bytes.SplitN(fd, newline, 4)
	dateTime, _ := time.Parse("2 Jan 2006", string(blogparts[1]))
	return BlogEntry{
		Title:    string(blogparts[0]),
		Date:     string(blogparts[1]),
		DateTime: dateTime,
		Loc:      filepath.Base(loc),
		Short:    string(blogparts[2]),
		Content:  string(blogparts[3]),
	}
}

func LoadEntrys(contentDirectory string) []BlogEntry {
	ents := []BlogEntry{}
	filepath.Walk(path.Join(contentDirectory, "blog"), func(path string, info os.FileInfo, err error) error {
		if !strings.HasSuffix(path, ".html") {
			return nil
		}
		ents = append(ents, loadEntry(path))
		return nil
	})
	sort.Slice(ents, func(i, j int) bool {
		return ents[i].DateTime.After(ents[j].DateTime)
	})
	return ents
}
